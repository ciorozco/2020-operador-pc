CURSO: Operador de PC
=============

Docentes: Abad Martín Gabriel. <br>
Coordinadores: Dr. Cristian Martínez. Lic. Carlos Ismael Orozco

Acerca del curso: El mismo forma parte de una vinculación entre el Departamento de Informática ([DIUNSa](http://di.unsa.edu.ar)) y la Escuela de Arte y Oficio de la Provincia de Salta.

**Temario**

[[_TOC_]]

# Semana 1: Introducción
Clase 1: [Presentación](https://gitlab.com/ciorozco/2020-operador-pc/-/blob/master/ShortOrozco_2020.pdf) <br>
Clase 2: Correo Electrónico

# Semana 2
